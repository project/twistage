<?php

/**
 * Implementation of hook_menu, for admin content type selection form.
 */
function twistage_publish_menu($may_cache) {
  $items = array();
  
  $items[] = array(
    'path' => 'admin/settings/twistage/publishing',
    'title' => t('Publishing settings'),
    'type' => MENU_CALLBACK,
    'callback' => 'drupal_get_form',
    'callback arguments' => array('twistage_publish_admin_form', arg(4)),
    'access' => user_access('administer twistage'),
  );
  
  return $items;
}

/**
 * Implementation of hook_form_alter, to add the video submission fieldset to the node form.
 */
function twistage_publish_form_alter($form_id, &$form) {
  if (isset($form['type'])) {
    $type = $form['type']['#value'];
  } else {
    return;
  }
  
  $avail_profiles = array();
  $result = db_query("SELECT * FROM {twistage_profiles} WHERE 1");
  while ($profile = db_fetch_object($result)) {
    $types = unserialize($profile->publish_types);
    if ($types[$type]) {
      $avail_profiles[$profile->pid] = $profile->name;
    }
  }
  
  if (!$avail_profiles) {
    return;
  }
  
  drupal_add_js(drupal_get_path('module','twistage') . '/twistage_publish.js');
  
  // Add elements for existing videos
  $videos = twistage_publish_get_videos_by_nid($form['#node']->nid);
  if ($videos) {
    $form['twistage_edit'] = array(
      '#type' => 'fieldset',
      '#title' => t('Maintain videos'),
      '#collapsible' => true,
      '#tree' => true,
    );
    
    foreach($videos as $video) {
      $form['twistage_edit'][$video->vid] = twistage_publish_form_element($avail_profiles, 0, $video);
    }
  }
  
  // Allow now video uploads.
  
  // The hidden value of twistage_sequence is manipulated by jQuery as the user adds videos on the fly.
  // This is somewhat unorthodox, but we can "cheat" and look at the post vars to see how many internal forms API members of this array to create,
  // thereby giving us access to them in hook_nodeapi later on.
  $seq = $_POST['twistage_sequence'] ? $_POST['twistage_sequence'] : 0;
    
  $form['twistage_sequence'] = array(
    '#type' => 'hidden',
    '#value' => $seq,
  );
  
  $form['twistage_upload'] = array(
	  '#tree' => true,
	  '#prefix' => '<div id="twistage-add-replicate">',
	  '#suffix' => '</div>',
  );
  
  for($i = 0; $i <= $seq; $i++) {
    $elem = array(
      '#type' => 'fieldset',
      '#title' => t('Video Upload'),
      '#collapsible' => true,
    );
    
    if ($i == 0) {
      $elem['#attributes'] = array('class' => 'twistage-add-original');
    }

    $form['twistage_upload'][$i] = array_merge($elem, twistage_publish_form_element($avail_profiles, $seq));
  }
  
  
}

/**
 * Implementation of hook_nodeapi
 */
function twistage_publish_nodeapi(&$node, $op, $teaser, $page) {
  switch($op) {
    case 'load':
      $result = db_query("SELECT * FROM {twistage_video_node} WHERE nid=%d", $node->nid);
      $node->twistage_video = array();
      while ($link = db_fetch_object($result)) {
        $node->twistage_video[] = twistage_get_video($link->vid, $link->pid);
      }
    break;
    
    case 'validate':
      foreach ($node->twistage_edit as $vid => $edit) {
        if (!$edit['twistage_title']) {
          form_set_error('twistage_edit][' . $vid . '][twistage_title', t('Title is required'));
        }
      }
      
      foreach ($node->twistage_upload as $seq => $upload) {
        if (twistage_publish_upload_exists($seq) && !$upload['twistage_title']) {
          form_set_error('twistage_upload][' . $seq . '][twistage_title', t('Title is required'));
        }
      }
    break;
    
    case 'insert':
    case 'update':
      //print_r($node); print_r($_FILES); die();
      // The twistage_video property is set by hook_nodeapi on load only, so its our indicator of whether a video already exists.
      // All the other properties are set only in the node form.
      // This half of this if statement handles the case whereby a video exists, has not been overwritten, but some metadata may have been changed.
      
      foreach ($node->twistage_edit as $vid => $edit) {
      //if ($node->twistage_video && !twistage_publish_upload_exists()) {
        $video = twistage_get_video($vid, $edit['twistage_pid']);
        $query = '';
        
        $availability = $edit['twistage_availability'] ? "available" : "hidden";
        if ($availability != $video->availability) {
          $query .=  "&video[available_for_display]=" . $availability;
        }
        
        if ($edit['twistage_title'] != $video->title) {
           $query .= "&video[title]=" . urlencode($edit['twistage_title']);
        }
        
        if ($edit['description'] != $video->description) {
           $query .= "&video[description]=" . urlencode($edit['twistage_description']);
        }
        
        // An unfortunate workaround for the fact that trim() does not operate on arrays.
        $in = explode(',', $edit['twistage_tags']);
        $tags = array();
        foreach ($in as $tag) {
          $tags[] = trim($tag);
        }
        
        if ($tags != $video->tags) {
          $send = array();
          foreach ($tags as $tag) {
            $send[] = urlencode($tag);
          }
          $query .= "&video[tag_list]=" . implode(',', $send);
        }
        
        // Video exists; check to see if we need to turn it off via the make avail. checkbox
        if ($query) {
          $profile = twistage_get_profile($video->pid);
          $signature = _twistage_authenticate($profile->site_key, $profile->site_username);
          $url = "http://service.twistage.com./videos/" . $video->vid . "?signature=" . urlencode($signature) . $query;
          $f = tmpfile();
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_PUT, true);
          curl_setopt($ch, CURLOPT_INFILE, $f);
          curl_setopt($ch, CURLOPT_INFILESIZE, 0);
          curl_setopt($ch, CURLOPT_TIMEOUT, 900);
          curl_setopt($ch, CURLOPT_CONNECTIONTIMEOUT, 30);
          curl_setopt($ch, CURLOPT_FAILONERROR, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $ret = curl_exec($ch);
          fclose($f);
        }
      // This half of the if statement handles adding a new video to Twistage.
      // It doesn't matter here if an old video exists -- the node-video association table will be updated in hook_twistage_api below
      // after the video is done processing/transcoding. The old video will just remain stored on Twistage until an admin chooses to delete it.
      }
      // TODO: perform some cursory check on MIME types before forging ahead with the video
      
      // This will be filled in the for loop.
      $elements = array('add' => array('publish-profile' => 'development', 'list' => array()));
      $proc = false;
      
      foreach ($node->twistage_upload as $seq => $upload) {
        // Break out if there's no upload.
        if (!twistage_publish_upload_exists($seq)) {
          continue;
        }
        $proc = true;
        
        // First, move the uploaded file to a temp files directory, since Twistage must get the video over HTTP.
        if (!is_dir(file_directory_path() . '/twistage_temp')) {
          mkdir(file_directory_path() . '/twistage_temp');
        }
        $info = file_save_upload('twistage_upload._' . $seq, file_directory_path() . '/twistage_temp');
        $url = url($info->filepath, NULL, NULL, TRUE);
        
        $tags = explode(',', $upload['twistage_tags']);
        $tags_arr = array();
        foreach ($tags as $t) {
          $tags_arr[] = array('key' => 'tag', 'value' => trim($t));
        }
        
        // Create an array to represent the XML we must submit to Twistage. 
        $elements['add']['list'][] = array(
          'key' => 'entry',
          'value' => array(
            'src' => $url,
            'title' => $upload['twistage_title'],
            'description' => $upload['twistage_description'],
            'tags' => $tags_arr,
            'customdata' => array(
              'nid' => $node->nid,
            ),
          ),
        );
      }
      if ($proc) {
        $profile = twistage_get_profile($upload['twistage_pid']);
        $signature = _twistage_authenticate($profile->site_key, $profile->site_username);
        $post = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . format_xml_elements($elements);
        $header = array("Content-type: text/xml");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://service.twistage.com/videos/create_many?signature=" . $signature);
        curl_setopt($ch, CURLOPT_TIMEOUT, 900);
        curl_setopt($ch, CURLOPT_CONNECTIONTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $ret = curl_exec($ch);
        print_r($post);
        print "ret: $ret\n";
        curl_close($ch); 
      }
    break;
    
    case 'view':
      // Make the video available in the node display.
      $node->content['twistage_video'] = array('#value' => '', '#weight' => -1);
      foreach($node->twistage_video as $video) {
        $profile = twistage_get_profile($video->pid);
        $preroll = twistage_get_preroll_video($video->pid);
        // theme_twistage_video will return nothing if the video is still processing or has been marked unavailable.
        // The default weight will place it just above the node body, but of course themes can override this and show the video code wherever they like.
        $node->content['twistage_video']['#value'] .= theme('twistage_video', $video, $preroll, $profile->preroll_url, $profile->width, $profile->height, false);
      }
    break;
  }
}

/**
 * Implementation of hook_twistage_api (see twistage.module)
 * Note that here we will get $video->nid in the event that it was passed down from Twistage.
 * This indicates we've seeing the results of a one-click publish when we submitted the video back in hook_nodeapi.
 */
function twistage_publish_twistage_api($op, $video) {
  // All we're interested in here is to see whether we need to maintain/delete node/video associations.
  switch ($op) {
    case 'update':
    case 'create':
      if ($video->nid) {
        //db_query("DELETE FROM {twistage_video_node} WHERE nid=%d", $video->nid);
        db_query("INSERT INTO {twistage_video_node} (nid, pid, vid) VALUES (%d, %d, '%s')", $video->nid, $video->pid, $video->vid);
      }
    break;
    
    case 'delete':
      db_query("DELETE FROM {twistage_video_node} WHERE vid='%s' AND pid=%d", $video->vid, $video->pid);
    break; 
  }
}

function twistage_publish_admin_form($pid) {
  $form = array();
  
  $form['info'] = array(
    '#value' => t('Select the content types which will allow uploading to this Twistage site.'),
  );
  
  
  $result = db_query("SELECT type,name FROM {node_type} WHERE 1");
  $types = array();
  while ($type = db_fetch_object($result)) {
    $types[$type->type] = $type->name;
  }

  $profile = twistage_get_profile($pid);
  $selected = unserialize($profile->publish_types);  

  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $pid,
  );
  
  $form['types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#options' => $types,
    '#default_value' => $selected
  );
  
  return system_settings_form($form);
}

function twistage_publish_admin_form_submit($form_id, $form_values) {
  $profile = twistage_get_profile($form_values['pid']);
  db_query("UPDATE {twistage_profiles} SET publish_types='%s' WHERE pid=%d", serialize($form_values['types']), $form_values['pid']);
  return 'admin/settings/twistage';
}

function twistage_publish_upload_exists($seq) {
  if ($_FILES['files']['name']['twistage_upload_' . $seq]) {
    return true;
  } else {
    return false;
  }
}

function twistage_publish_get_video_by_nid($nid) {
  $result = db_fetch_object(db_query("SELECT * FROM {twistage_video_node} WHERE nid=%d", $nid));
  return twistage_get_video($result->vid, $result->pid);
}

function twistage_publish_get_videos_by_nid($nid) {
  $result = db_query("SELECT * FROM {twistage_video_node} WHERE nid=%d", $nid);
  $videos = array();
  while ($video = db_fetch_object($result)) {
    $videos[] = $video;
  }
  return $videos;
}

function twistage_publish_form_element($profiles, $seq = 0, $video = NULL) {
  $form = array();
  
  $form = array(
    '#tree' => TRUE,
  );

  $form['notice'] = array(
    '#value' => t('<p><b>Video note</b>: All video uploads and edits must be sent to Twistage and processed before taking effect locally. There will be a brief delay before changes are visible locally.'),
  );

  $avail = $video->availability == 'available' ? true : false;
  $form['twistage_availability'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make video available for viewing'),
    '#default_value' => ($video) ? $avail : true,
  );

  $form['twistage_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Video Title'),
    '#default_value' => $video->title,
  );

  $form['twistage_tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Video Tags'),
    '#description' => t('Separate tags with commas'),
    '#default_value' => implode(',', $video->tags),
  );

  $form['twistage_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Video Description'),
    '#default_value' => $video->description,
  );

  if (!$video) {
    // These fields are only to be shown on nodes with no prior video uploaded; we don't want the user thinking they can change them later.
    $form['twistage_pid'] = array(
      '#type' => 'select',
      '#title' => t('Profile'),
      '#description' => t('Select the Twistage site profile to add this video to.'),
      '#options' => $profiles,
    );
  
    $form['twistage_file_0'] = array(
      '#type' => 'file',
      '#title' => t('Select Video File'),
    );
  
    $form['more'] = array(
      '#value' => '<a class="twistage-add-more">Add another video...</a>',
    );
  } else {
    // Get current video name (also the nid of this node will be available, since existing video means existing node)
    $existing = $video;
    $existing_profile = twistage_get_profile($existing->pid);
    $form['info'] = array(
      '#value' => t('<p><b>Current video</b>: @title</p>', array('@title' => $existing->title)),
    );
    $form['info2'] = array(
      '#value' => t('<p><b>Current profile</b>: @profile</p>', array('@profile' => $existing_profile->name)),
    );
    
    $form['twistage_vid'] = array(
      '#type' => 'hidden',
      '#value' => $video->vid,
    );
  
    /*$form['twistage_file_0'] = array(
      '#type' => 'file',
      '#title' => t('Upload a new video'),
      '#description' => t('This will overwrite the old video. Leave this field blank to keep the old video as-is.'),
    );*/
  }
  
  return $form;
}